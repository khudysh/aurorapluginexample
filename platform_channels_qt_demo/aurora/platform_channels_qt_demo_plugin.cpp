#include <platform_channels_qt_demo/platform_channels_qt_demo_plugin.h>

namespace Channels
{
    constexpr auto Event = "platform_channels_qt_demo";
} // namespace Channels


void PlatformChannelsQtDemoPlugin::RegisterWithRegistrar(flutter::PluginRegistrar *registrar)
{
    // Создайте EventChannel с помощью StandardMethodCodec
    auto eventChannel = std::make_unique<EventChannel>(
        registrar->messenger(), Channels::Event,
        &flutter::StandardMethodCodec::GetInstance());

    // Создать плагин
    std::unique_ptr<PlatformChannelsQtDemoPlugin> plugin(
        new PlatformChannelsQtDemoPlugin(std::move(eventChannel)));

    // Зарегистрировать плагин
    registrar->AddPlugin(std::move(plugin));
}

PlatformChannelsQtDemoPlugin::PlatformChannelsQtDemoPlugin(std::unique_ptr<EventChannel> eventChannel) 
: m_eventChannel(std::move(eventChannel)), m_settings("MyCompany", "MyApp")
{
    // Создать StreamHandler
    RegisterStreamHandler();
}

void PlatformChannelsQtDemoPlugin::RegisterStreamHandler()
{
    // Создать обработчик событий Platform Channels
    auto handler = std::make_unique<flutter::StreamHandlerFunctions<EncodableValue>>(
        [&](const EncodableValue *,
            std::unique_ptr<flutter::EventSink<EncodableValue>> &&events) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>>
        {
            m_sink = std::move(events);
            onEventChannelEnable();
            return nullptr;
        },
        [&](const EncodableValue *) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>>
        {
            onEventChannelDisable();
            return nullptr;
        });

    // Зарегистрировать событие
    m_eventChannel->SetStreamHandler(std::move(handler));
}

void PlatformChannelsQtDemoPlugin::onEventChannelEnable() {
    // Подключить соединение для прослушивания изменений в QSettings
    m_valueChangedConnection = QObject::connect(&m_settings, &QSettings::valueChanged, this, &PlatformChannelsQtDemoPlugin::onEventChannelSend);
}

void PlatformChannelsQtDemoPlugin::onEventChannelDisable() {
    // Отключить соединение для прослушивания
    QObject::disconnect(m_valueChangedConnection);
}

void PlatformChannelsQtDemoPlugin::onEventChannelSend() {
    // Предполагаем, что измененное значение последнее
    QString key = m_settings.allKeys().last();
    QString newValue = m_settings.value(key).toString();
    EncodableMap message;
    message[EncodableValue("key")] = EncodableValue(key.toStdString());
    message[EncodableValue("value")] = EncodableValue(newValue.toStdString());
    m_sink->Success(message);
}

void PlatformChannelsQtDemoPlugin::saveString(const QString& key, const QString& value) {
    m_settings.setValue(key, value);
}

QString PlatformChannelsQtDemoPlugin::loadString(const QString& key) const {
    return m_settings.value(key).toString();
}

// void PlatformChannelsQtDemoPlugin::onEventChannelEnable()
// {
//     // Отправить после запуска
//     onEventChannelSend();
//     // Подключить соединение для прослушивания
//     m_onlineStateChangedConnection =
//         QObject::connect(&m_manager,
//                          &QNetworkConfigurationManager::onlineStateChanged,
//                          this,
//                          &PlatformChannelsQtDemoPlugin::onEventChannelSend);
// }

// void PlatformChannelsQtDemoPlugin::onEventChannelDisable()
// {
//     // Отключить соединение для прослушивания
//     QObject::disconnect(m_onlineStateChangedConnection);
// }

// void PlatformChannelsQtDemoPlugin::onEventChannelSend()
// {
//     // Отправить состояние если были изменения
//     auto state = m_manager.isOnline();
//     if (state != m_state)
//     {
//         m_state = state;
//         m_sink->Success(m_manager.isOnline());
//     }
// }

// Добавить мок-файл
#include "moc_platform_channels_qt_demo_plugin.cpp"