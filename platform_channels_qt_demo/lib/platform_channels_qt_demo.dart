import 'platform_channels_qt_demo_platform_interface.dart';

class PlatformChannelsQtDemo {
  Stream<String?> stateNetworkConnect() {
    return PlatformChannelsQtDemoPlatform.instance.stateNetworkConnect();
  }
}
