#ifndef FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H
#define FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H

#include <platform_channels_qt_demo/globals.h>

// Добавить зависимость Qt
#include <QNetworkConfigurationManager>
#include <QSettings>
#include <QString>

#include <flutter/plugin_registrar.h>
#include <flutter/encodable_value.h>
#include <flutter/standard_method_codec.h>
#include <flutter/event_channel.h>
#include <flutter/event_stream_handler_functions.h>

// Flutter encodable
typedef flutter::EncodableValue EncodableValue;
typedef flutter::EncodableMap EncodableMap;
typedef flutter::EncodableList EncodableList;
// Flutter события
typedef flutter::EventChannel<EncodableValue> EventChannel;
typedef flutter::EventSink<EncodableValue> EventSink;

// Включить QObject
class PLUGIN_EXPORT PlatformChannelsQtDemoPlugin final
    : public QObject
    , public flutter::Plugin
{
    Q_OBJECT
public:
    static void RegisterWithRegistrar(flutter::PluginRegistrar* registrar);

// Создать слот
public slots:
    void onEventChannelSend();

private:
    // Создает плагин, который взаимодействует по данному каналу.
    PlatformChannelsQtDemoPlugin(
        std::unique_ptr<EventChannel> eventChannel
    );

    // Метод регистрируют обработчики каналов
    void RegisterStreamHandler();

    // Другие методы
    void onEventChannelEnable();
    void onEventChannelDisable();

    // Переменные Flutter
    std::unique_ptr<EventChannel> m_eventChannel;
    std::unique_ptr<EventSink> m_sink;

    // Переменные Qt
    bool m_state;
    QNetworkConfigurationManager m_manager;
    QMetaObject::Connection m_onlineStateChangedConnection;
};

#endif /* FLUTTER_PLUGIN_PLATFORM_CHANNELS_QT_DEMO_PLUGIN_H */