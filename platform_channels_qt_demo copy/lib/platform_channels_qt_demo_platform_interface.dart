import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'platform_channels_qt_demo_method_channel.dart';

abstract class PlatformChannelsQtDemoPlatform extends PlatformInterface {
  /// Constructs a PlatformChannelsQtDemoPlatform.
  PlatformChannelsQtDemoPlatform() : super(token: _token);

  /// ...
  Stream<String?> stateNetworkConnect() {
    throw UnimplementedError('connectNetworkState() has not been implemented.');
  }

  static final Object _token = Object();

  static PlatformChannelsQtDemoPlatform _instance =
      MethodChannelPlatformChannelsQtDemo();

  /// The default instance of [PlatformChannelsQtDemoPlatform] to use.
  ///
  /// Defaults to [MethodChannelPlatformChannelsQtDemo].
  static PlatformChannelsQtDemoPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [PlatformChannelsQtDemoPlatform] when
  /// they register themselves.
  static set instance(PlatformChannelsQtDemoPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
