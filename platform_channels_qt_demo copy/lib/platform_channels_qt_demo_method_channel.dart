import 'package:flutter/services.dart';
import 'platform_channels_qt_demo_platform_interface.dart';

// Ключ канала плагина
const channelEvent = "platform_channels_qt_demo";

/// Реализация [PlatformChannelsQtDemoPlatform], использующая каналы методов.
class MethodChannelPlatformChannelsQtDemo
    extends PlatformChannelsQtDemoPlatform {
  /// Канал метода, используемый для взаимодействия с собственной платформой.
  final eventChannel = const EventChannel(channelEvent);

  /// Отображение состояния сети с помощью EventChannel
  @override
  Stream<String?> stateNetworkConnect() {
    return eventChannel
        .receiveBroadcastStream()
        .map((event) => event as String);
  }
}
