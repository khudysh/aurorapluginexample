import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'settings_plugin_method_channel.dart';

abstract class SettingsPluginPlatform extends PlatformInterface {
  /// Constructs a SettingsPluginPlatform.
  SettingsPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static SettingsPluginPlatform _instance = MethodChannelSettingsPlugin();

  /// The default instance of [SettingsPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelSettingsPlugin].
  static SettingsPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SettingsPluginPlatform] when
  /// they register themselves.
  static set instance(SettingsPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
