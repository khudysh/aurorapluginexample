
import 'settings_plugin_platform_interface.dart';

class SettingsPlugin {
  Future<String?> getPlatformVersion() {
    return SettingsPluginPlatform.instance.getPlatformVersion();
  }
}
