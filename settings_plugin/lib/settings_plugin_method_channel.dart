import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'settings_plugin_platform_interface.dart';

/// An implementation of [SettingsPluginPlatform] that uses method channels.
class MethodChannelSettingsPlugin extends SettingsPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('settings_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
