import 'package:flutter_test/flutter_test.dart';
import 'package:settings_plugin/settings_plugin.dart';
import 'package:settings_plugin/settings_plugin_platform_interface.dart';
import 'package:settings_plugin/settings_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockSettingsPluginPlatform
    with MockPlatformInterfaceMixin
    implements SettingsPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final SettingsPluginPlatform initialPlatform = SettingsPluginPlatform.instance;

  test('$MethodChannelSettingsPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelSettingsPlugin>());
  });

  test('getPlatformVersion', () async {
    SettingsPlugin settingsPlugin = SettingsPlugin();
    MockSettingsPluginPlatform fakePlatform = MockSettingsPluginPlatform();
    SettingsPluginPlatform.instance = fakePlatform;

    expect(await settingsPlugin.getPlatformVersion(), '42');
  });
}
