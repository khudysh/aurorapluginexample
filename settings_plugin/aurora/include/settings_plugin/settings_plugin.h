#ifndef FLUTTER_PLUGIN_SETTINGS_PLUGIN_H
#define FLUTTER_PLUGIN_SETTINGS_PLUGIN_H

#include <settings_plugin/globals.h>

#include <flutter/plugin_registrar.h>
#include <flutter/encodable_value.h>
#include <flutter/method_channel.h>
#include <flutter/standard_method_codec.h>

#include <QObject>
#include <QSettings>
#include <QString>

// Flutter encodable
typedef flutter::EncodableValue EncodableValue;
typedef flutter::EncodableMap EncodableMap;
typedef flutter::EncodableList EncodableList;
// Flutter methods
typedef flutter::MethodChannel<EncodableValue> MethodChannel;
typedef flutter::MethodCall<EncodableValue> MethodCall;
typedef flutter::MethodResult<EncodableValue> MethodResult;

class SettingsPlugin : public QObject, public flutter::Plugin {
  Q_OBJECT

 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrar* registrar);

  SettingsPlugin();
  virtual ~SettingsPlugin();

 private slots:
  void onSettingChanged();

 private:
  // Method Call handler
  void HandleMethodCall(const flutter::MethodCall<flutter::EncodableValue>& call,
                        std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);

  // Settings object
  QSettings settings_;
  // Flutter method channel
  std::unique_ptr<flutter::MethodChannel<flutter::EncodableValue>> channel_;
};

#endif /* FLUTTER_PLUGIN_SETTINGS_PLUGIN_H */
