#include <settings_plugin/settings_plugin.h>

namespace Channels {
constexpr auto Methods = "settings_plugin";
} // namespace Channels

namespace Methods {
constexpr auto PlatformVersion = "getPlatformVersion";
} // namespace Methods

namespace {
const char kChannelName[] = "settings_plugin";
const char kSaveMethod[] = "saveSetting";
const char kListenMethod[] = "listenSetting";
const char kSettingKey[] = "setting_key";
const char kSettingValue[] = "setting_value";
}  // namespace

void SettingsPlugin::RegisterWithRegistrar(flutter::PluginRegistrar* registrar) {
  auto channel =
      std::make_unique<flutter::MethodChannel<flutter::EncodableValue>>(
          registrar->messenger(), kChannelName,
          &flutter::StandardMethodCodec::GetInstance());

  auto plugin = std::make_unique<SettingsPlugin>();

  channel->SetMethodCallHandler(
      [plugin_pointer = plugin.get()](const auto& call, auto result) {
        plugin_pointer->HandleMethodCall(call, std::move(result));
      });

  plugin->channel_ = std::move(channel);
  registrar->AddPlugin(std::move(plugin));
}

SettingsPlugin::SettingsPlugin() : settings_("MyCompany", "MyApp") {
  connect(&settings_, &QSettings::valueChanged, this, &SettingsPlugin::onSettingChanged);
}

SettingsPlugin::~SettingsPlugin() {}

void SettingsPlugin::HandleMethodCall(
    const flutter::MethodCall<flutter::EncodableValue>& call,
    std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result) {
  if (call.method_name().compare(kSaveMethod) == 0) {
    if (const auto* args = std::get_if<flutter::EncodableMap>(call.arguments())) {
      auto key = QString::fromStdString(std::get<std::string>((*args)[flutter::EncodableValue(kSettingKey)]));
      auto value = QString::fromStdString(std::get<std::string>((*args)[flutter::EncodableValue(kSettingValue)]));
      settings_.setValue(key, value);
      result->Success();
    } else {
      result->Error("Bad Arguments", "Expected a map with key and value.");
    }
  } else if (call.method_name().compare(kListenMethod) == 0) {
    // No additional action required for listening setup
    result->Success();
  } else {
    result->NotImplemented();
  }
}

void SettingsPlugin::onSettingChanged() {
  QString key = sender()->objectName();
  QVariant value = settings_.value(key);
  flutter::EncodableMap response;
  response[flutter::EncodableValue(kSettingKey)] = flutter::EncodableValue(key.toStdString());
  response[flutter::EncodableValue(kSettingValue)] = flutter::EncodableValue(value.toString().toStdString());
  channel_->InvokeMethod("onSettingChanged", std::make_unique<flutter::EncodableValue>(response));
}

#include "moc_settings_plugin.cpp"